using UnityEngine;
using System.Collections;

public class Knight : Monster {
	public AnimationCurve walk;
	public enum States{Still,Prepare,Swing,Charge};
	public float walkTime, endTime, soundTime;
	public States state;
	public Collider swordHit;
	Vector3 turndirection = Vector3.zero;
	public Light lightGlow;
	public AudioClip walkSound, SwingSound, wakeSound;
	// Use this for initialization
		override public void Start () {
		base.Start ();
		state = States.Charge;
		triggered = false;
	}
	
	override public void trigger()
	{
		base.trigger (); 
		state = States.Charge;
		endTime = Time.time + walkTime;
			animation.CrossFade("AC_walk",1f);
		soundTime = Time.time+1.3f;
		MusicManager.Instance.PlayTrack(1);
		lightGlow.color = Color.red;
	}
	
	private void OnTriggerEnter(Collider c) {
		if (c.CompareTag("Player"))
		{
			if (triggered){
				state = States.Swing;
				animation.CrossFade("AC_slash");
				this.audio.PlayOneShot (SwingSound);
			}
		}
	}

	override public void Update()
	{
		performAction ();
	}
	//walk 3 seconds -> prepare -> walk
	override public void performAction()
	{
		if (triggered){
			switch(state)
			{
			case States.Swing: 
			if (!animation.IsPlaying("AC_slash"))
			{
				
				state = States.Charge;
				soundTime = Time.time+1.3f;
				turndirection = Vector3.zero;
				animation.CrossFade ("AC_walk",2f);
			}
			break;
			case States.Charge:
				float dTime = (this.animation["AC_walk"].time % this.animation["AC_walk"].length) /this.animation["AC_walk"].length;
				if (!this.audio.isPlaying && Time.time > soundTime) {
					this.audio.Play();
					soundTime = Time.time+1.3f;
				}
				this.transform.Translate ((turndirection*0.01f)*walk.Evaluate (dTime));
			break; 
			case States.Prepare: 
			//ANIMATE PREPARATION
			break;
			default: break;	
		}
			turndirection = GameObject.FindGameObjectWithTag("Player").transform.position - this.transform.position; 
			turndirection = new Vector3(turndirection.x,0,turndirection.z);
				if (turndirection != Vector3.zero)
			transform.forward = Vector3.Lerp (transform.forward,turndirection,(0.5f)*Time.deltaTime);
		}
	}
}
