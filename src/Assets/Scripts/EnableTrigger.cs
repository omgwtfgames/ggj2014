using UnityEngine;
using System.Collections;

public class EnableTrigger : MonoBehaviour {
	public GameObject target;
	public Knight kTarget;
	void OnTriggerEnter(Collider c) {
		if (c.CompareTag("Player"))
		{
			if (kTarget != null) kTarget.trigger ();
			if (target != null)
				if (!target.activeSelf) 
					target.SetActive (true);
			
			this.gameObject.SetActive (false);
			this.collider.enabled = false;
		}
		
	}
}
