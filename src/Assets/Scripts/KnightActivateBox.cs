using UnityEngine;
using System.Collections;

public class KnightActivateBox : MonoBehaviour {
	public Knight knight;
	
	//Check if the player  has reached the Swing distance, if so, swing at player.
	void OnTriggerEnter(Collider c) {
		if (c.CompareTag("Player"))
		{
			knight.state = Knight.States.Prepare;
			knight.triggered = true;
		}
	}
}
