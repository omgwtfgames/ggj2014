using UnityEngine;
using System.Collections;

public class EndScript : MonoBehaviour {
	public GameObject textLayer;
	void OnTriggerEnter(Collider c)
	{
		if (c.CompareTag("Player"))
		{
			textLayer.SetActive(true);
		}
	}
}
