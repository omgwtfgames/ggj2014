using UnityEngine;
using System.Collections;

public class Monster : MonoBehaviour {
	public bool triggered;
	// Use this for initialization
	public virtual void Start () {
		triggered = false;
		if (Network.isServer) {
			if (GetComponentsInChildren<SkinnedMeshRenderer>() != null)
				foreach (SkinnedMeshRenderer ren in GetComponentsInChildren<SkinnedMeshRenderer>())
					ren.enabled = false;
		}	
	}
	
	//Check collider, if it's a client player, trigger Monster to do stuff
	void OnTriggerEnter(Collider c) {
		if (c.CompareTag("Player") && Network.isClient)
			{
			trigger ();
		}	
	}
	
	//Trigger the monster
	public virtual void trigger()
	{
		triggered = true;
	}
	
	//Perform action
	public virtual void performAction() {}
	
	// Update is called once per frame
	//		if triggered, do stuff
	public virtual void Update () {
		if (triggered)
			performAction ();
	}
}
