using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	GameObject gameOver;
	bool _dying = false;
	// Use this for initialization
	void Start () {
	}
	
	void OnTriggerEnter(Collider c) {
		if (c.CompareTag("Monster") && (Network.isServer || 
			(Application.platform == RuntimePlatform.WindowsEditor && !Connect.Instance.emulateServer))) {
			Die();
		}
	}
	
	void Die()
	{
		/*
		if (_dying) return;
		_dying = true;
		Debug.Log("You have died");
		StartCoroutine(DieAsync());
		*/
	}
	
	IEnumerator DieAsync() {
		// show blood overlay
		GameObject.Find("EffectCam").SendMessage("FadeInTexture");
		//if (gameOver != null) gameOver.SetActive(true);
		
		/*
		GameObject gameOver;
		if (transform.root.Find("OVRCameraController/GameOver/mod_gameover") != null) {
			gameOver = transform.root.Find("OVRCameraController/GameOver/mod_gameover").gameObject;
			gameOver.SetActive(true);
	    }
		if (transform.Find("OVRCameraController/GameOver/mod_gameover") != null) {
			gameOver = transform.Find("OVRCameraController/GameOver/mod_gameover").gameObject;
		    gameOver.SetActive(true);
		}
		*/
		yield return new WaitForSeconds(5.0f);
		
		if (Network.isServer) GameManager.Instance.networkView.RPC("ReloadLevel", RPCMode.Others);
		Application.LoadLevel("game");
	}
	
	// Update is called once per frame
	void Update () {
		//If the player has fallen (presuming that the player should not go below y = -5)
		if (this.transform.position.y <= -5)
		{
			Die();
		}
	}
}
