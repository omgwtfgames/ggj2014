using UnityEngine;
using System.Collections;

public class Connect : MonoBehaviour {
	public static string serverIP = "";
	public bool emulateServer = false;
	public static bool isServer = false;
	public int port = 8666;
	public GameObject playerControllerPrefab;
	public GameObject ovrCameraPrefab;
	public Transform playerTransform;
	public static Connect Instance;
	
    void Awake() {
		// some kind of singleton
		if (Instance == null) {
			Instance = this;
		} else {
			DestroyImmediate(gameObject);
			return;
		}
		DontDestroyOnLoad(gameObject);
		
		if (isServer) {
			Network.InitializeServer(10, port, false);
			if (playerTransform == null) {
				playerTransform = (Network.Instantiate(playerControllerPrefab, 
					                                   Vector3.zero, 
				    	                               Quaternion.identity, 0) as GameObject).transform;
			}
			playerTransform.GetComponent<OVRMainMenu>().enabled = false;
		} else {
			Network.Connect(serverIP, port);
			playerTransform.GetComponent<OVRPlayerController>().ControlsEnabled = false;
		}
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnPlayerConnected(NetworkPlayer player) {
        Debug.Log("Player  connected from " + player.ipAddress + ":" + player.port);
		playerTransform.gameObject.SetActive(true);
    }
	
	void OnConnectedToServer() {
        Debug.Log("Connected to server");
		//oculusCamera.SendMessage("BecomeOwner");
		//oculusCamera.transform.Find("CameraRight").SendMessage("BecomeOwner");
		if (!GameObject.FindObjectOfType(typeof(OVRCameraController))) {
			GameObject ovrCam = Network.Instantiate(ovrCameraPrefab, playerTransform.position, Quaternion.identity, 0) as GameObject;
			ovrCam.transform.parent = playerTransform;
			//ovrCam.name = "OvrCameraController";
			playerTransform.gameObject.SetActive(true);
			playerTransform.GetComponent<OVRMainMenu>().enabled = true;
		}
		//Disable movement controls
    }
}
