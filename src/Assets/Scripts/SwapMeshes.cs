using UnityEngine;
using System.Collections;

public class SwapMeshes : MonoBehaviour {
	public Mesh darkMesh, lightMesh;
	public Texture darkTex, lightTex;
	// Use this for initialization
	void Start () {
		if (this.GetComponent<MeshFilter>() != null)
		{
				
		if (Network.isServer)
			{
			//Make Light
				if (lightMesh != null)
					GetComponent<MeshFilter>().sharedMesh = lightMesh;
				if (lightTex != null)
					this.renderer.material.mainTexture=lightTex;
			}
		else {
			//Make Dark;
				if (darkMesh != null)
					GetComponent<MeshFilter>().sharedMesh = darkMesh;	
				if (darkTex != null)
					this.renderer.material.mainTexture=darkTex;
			}
						
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
