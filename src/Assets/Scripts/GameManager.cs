using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
	
	public int spawnPointIndex = 0;
	public List<SpawnPoint> spawnPoints;
	
	public bool win = false;
	
	public static GameManager Instance;
	
	void OnLevelWasLoaded(int level) {
		GameObject.FindGameObjectWithTag("Player").transform.position = spawnPoints[spawnPointIndex].transform.position;
		Debug.Log("Respawning at point: " + spawnPointIndex.ToString() + " " + spawnPoints[spawnPointIndex].transform.position);
	}
	
	// Use this for initialization
	void Awake () {
		if (Instance == null) {
			Instance = this;
		} else {
			DestroyImmediate(gameObject);
			return;
		}
		DontDestroyOnLoad(gameObject);
	}
	
	void Start() {
		MusicManager.Instance.PlayTrack(0);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
		if (Input.GetKeyDown(KeyCode.Backspace)) Application.LoadLevel("game");
	}
	
	[RPC]
	public void ReloadLevel() {
		Application.LoadLevel("game");
	}
}
