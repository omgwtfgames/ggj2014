using UnityEngine;
using System.Collections;

public class DisableRender : MonoBehaviour {
	public bool forServer;
	void Start () {
		if (Network.isServer && !forServer)
		{
			if (GetComponent<MeshRenderer>() != null)
				GetComponent<MeshRenderer>().enabled = false;
			if (GetComponentInChildren<MeshRenderer>() != null)
				GetComponentInChildren<MeshRenderer>().enabled = false;
		}
		else if (Network.isClient && forServer){
			if (GetComponent<MeshRenderer>() != null)
				GetComponent<MeshRenderer>().enabled = false;
			if (GetComponentInChildren<MeshRenderer>() != null)
				GetComponentInChildren<MeshRenderer>().enabled = false;
		}
	}
}
