using UnityEngine;
using System.Collections;

public class VisibleCheck: MonoBehaviour {
	public enum visibility{Server,Client};
	public visibility visible;
	// Use this for initialization
	void Start () {
		if (Network.isServer && visible != visibility.Server)
		{
			if (GetComponentInChildren<SkinnedMeshRenderer>() != null)
				GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
		if (this.renderer != null)
			this.renderer.enabled = false;
		}
		if (Network.isClient && visible != visibility.Client)
		{
			if (GetComponentInChildren<SkinnedMeshRenderer>() != null)
				GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
		if (this.renderer != null)
			this.renderer.enabled = false;
		}
	}
}
