using UnityEngine;
using System.Collections;

public class ForwardDirectionCameraSync : MonoBehaviour {

	public Transform oculusCameraController;
	public Transform oculusCameraRight;
	
	Transform _transform;
	
	void Awake() {
		_transform = transform;
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (oculusCameraController != null) {
			_transform.rotation = oculusCameraController.rotation;
		}
	}
}
