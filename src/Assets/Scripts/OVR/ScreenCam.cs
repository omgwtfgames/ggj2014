using UnityEngine;
using System.Collections;

public class ScreenCam : MonoBehaviour {
	public Transform oculusCameraController;
	public Transform oculusCameraRight;
	
	Transform _transform;
	
	void Awake() {
		_transform = transform;
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		_transform.eulerAngles.Set(oculusCameraRight.localRotation.eulerAngles.x, 
			                       oculusCameraController.localRotation.eulerAngles.y, 0f);
	
	}
}
