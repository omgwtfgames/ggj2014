using UnityEngine;
using System.Collections;

public class OvrCamDisableOnServer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		transform.parent = GameObject.FindGameObjectWithTag("Player").transform;
		if (Network.isServer) {
			GetComponent<OVRDevice>().enabled = false;
			transform.Find("CameraLeft").GetComponent<Camera>().enabled = false;
			transform.Find("CameraRight").GetComponent<Camera>().enabled = false;
			transform.Find("CameraRight").GetComponent<AudioListener>().enabled = false;
			//transform.parent.Find("ForwardDirection").
			//	GetComponent<ForwardDirectionCameraSync>().
			//		oculusCameraController = transform;
			GameObject.FindGameObjectWithTag("Player").GetComponent<OVRPlayerController>().Awake();
		} else {
			transform.Find("ScreenPlayerCamera").gameObject.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
