using UnityEngine;
using System.Collections;

public class SwordHit : MonoBehaviour {

	void OnTriggerEnter(Collider c)
	{
		if (c.CompareTag("Player") && Network.isServer )
		{
			GameObject.FindGameObjectWithTag("Player").SendMessage("Die");
		}
	}
}
