using UnityEngine;
using System.Collections;

public class EffectCam : MonoBehaviour {
	ScreenOverlay _overlay;
	bool fadeIn = false;
	
	// Use this for initialization
	void Start () {
	 	_overlay = GetComponent<ScreenOverlay>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (fadeIn && _overlay.intensity < 5f) {
			_overlay.intensity += Time.deltaTime;
		}
	}
	
	public void FadeInTexture() {
		fadeIn = true;
	}
}
