using UnityEngine;
using System.Collections;

public class OnlyInEditor : MonoBehaviour {

	void Awake () {
        #if !UNITY_EDITOR
		DestroyImmediate(gameObject);
        #endif
	}

}
