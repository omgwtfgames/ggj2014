using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider c) {
		if (c.CompareTag("Player")) {
			int hitSpawnIndex = GameManager.Instance.spawnPoints.IndexOf(this);
			if (hitSpawnIndex > GameManager.Instance.spawnPointIndex) {
				GameManager.Instance.spawnPointIndex = hitSpawnIndex;
			}
		}
	}
}
