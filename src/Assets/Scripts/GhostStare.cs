using UnityEngine;
using System.Collections;

public class GhostStare : Ghost {
	Vector3 turndirection = Vector3.zero;
	override public void performAction()
	{
		if (triggered)
		{
			if (isRaising)
			{
				if (animation["AC_rise"].enabled)
				{
					animation["AC_rise"].speed = 1;
				}
				else {isRaising = false; animation.CrossFade ("AC_hover",1f);}
				
			}
			else{
				turndirection = GameObject.FindGameObjectWithTag("Player").transform.position - this.transform.position; 
				turndirection = new Vector3(turndirection.x,-turndirection.y,turndirection.z);
				if (turndirection != Vector3.zero)
				transform.forward = Vector3.Lerp (transform.forward,turndirection,(1f)*Time.deltaTime);	
			}
		}
	}
}
