using UnityEngine;
using System.Collections;

public class ConnectionGUI : MonoBehaviour {
	string footServerIP = "";
	
	// Use this for initialization
	void Start () {
		Debug.Log ("My IP is " + Network.player.ipAddress);
		footServerIP = PlayerPrefs.GetString("lastFootIP", "127.0.0.1");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
	}
	
	void OnGUI() {
		GUI.Label(new Rect(10f, 
			               10f, 100f, 100f), "My IP is " + Network.player.ipAddress + " - Feet tells head to put this in the box");
		if (GUI.Button(new Rect(Screen.width/2, 
			                    Screen.height/2, 100f, 100f), "I'm the feet")) {
			Connect.isServer = true;
			Application.LoadLevel("game");
		}
		if (GUI.Button(new Rect(Screen.width/2, 
			                    Screen.height/2 - 100, 100f, 100f), "I'm the head")) {
			Connect.isServer = false;
			PlayerPrefs.SetString("lastFootIP", footServerIP);
			Application.LoadLevel("game");
		}
		footServerIP = GUI.TextField(new Rect(Screen.width/2, 
			                                  Screen.height/2 + 100f, 100f, 30f), footServerIP, 16);
		Connect.serverIP = footServerIP;
	}
}
