using UnityEngine;
using System.Collections;

public class DropKill : MonoBehaviour {
	void OnTriggerEnter(Collider c)
	{
		if (c.CompareTag("Player") && Network.isServer)
		{
			Debug.Log ("Dropped");
			GameObject.FindGameObjectWithTag("Player").SendMessage("Die");
		}
	}
}
