using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Drape : Monster {
	public GameObject model;
	public bool prop = true;
	
	override public void Start () {
		base.Start();
		//Place it on its first frame
		model.animation["AC_turn"].speed = 0;
		model.animation.Play("AC_turn");
		
	}
	override public void performAction()
	{
		if (prop) return;
		
		model.animation["AC_turn"].speed = 1;
		if (!model.animation["AC_turn"].enabled && !model.animation["AC_scare"].enabled)
		{
				model.animation.Play("AC_scare");
		}
	}
}
