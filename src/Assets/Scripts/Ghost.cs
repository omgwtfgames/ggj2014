using UnityEngine;
using System.Collections;
public class Ghost : Monster {
	float endTime;
	public float runTime, raiseTime = 1;
	public bool isRaising, isRunning, isDropping;
	public Vector3 motion;
	public Ghost[] GhostGroup;
	//Ghost uses a base motion speed which gets incremented as it chases the player, the
	// player must dodge it or eventually get overrun.
	//The speed increase could be fiddled with to make it a bit more balanced.
	
	
	//Multiply movement by Delta Time
	
	// Use this for initialization
	override public void Start () {
		base.Start();
		isRaising = false;
		isRunning = false;
		animation.CrossFade ("AC_rise",1f);
		animation["AC_rise"].speed = 0;
		
	}
	override public void trigger()
	{
		if ( triggered == false)
		{
			if (GhostGroup.Length > 0)
			{ 
				foreach (Ghost g in GhostGroup)
				{
					if (!g.triggered)
					{
						triggered = true;
						isRaising = true;
						endTime = Time.time + raiseTime;
						animation["AC_rise"].speed = 1;
						
						
					}
				}
			}
			else{
				triggered = true;
				isRaising = true;
				endTime = Time.time + raiseTime;
			}
		}
		if (triggered)
		{
				if (transform.Find ("Killsphere") != null)
				transform.Find ("Killsphere").gameObject.collider.enabled = true;	
				this.audio.Play();
		}
	}
	
	override public void performAction()
	{
		if (triggered)
		{
			float cTime = Time.time;
			if (isRaising)
			{
				if (animation["AC_rise"].enabled)
				{
					animation["AC_rise"].speed = 1;
				}
				else {endTime = cTime + runTime; isRaising = false; isRunning = true; animation.CrossFade ("AC_hover",1f);}
				
			}
			else if (isRunning)
			{
				if (cTime < endTime)
				{

					this.transform.Translate (motion);
					motion *= 1.003f;
				}
				else {
					animation.Stop ("AC_hover");
					endTime = cTime + raiseTime;
					isRunning = false;
					isDropping = true;
					animation["AC_rise"].speed = -1;
					animation["AC_rise"].time = animation["AC_rise"].length;
					animation.CrossFade ("AC_rise",1f);
				}
				
			}
			else if (isDropping)
				if (animation["AC_rise"].time == 0)
				{
					this.gameObject.SetActive(false);
				}
		}
	}
}
