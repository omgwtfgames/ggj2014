using UnityEngine;
using System.Collections;

public class Collectable : MonoBehaviour {
	Transform _transform;
	
	// Use this for initialization
	void Awake () {
		_transform = transform;
	}
	
	// Update is called once per frame
	void Update () {
		_transform.localEulerAngles += new Vector3(0f, 30f*Time.deltaTime, 0f);
	}
	
	void OnTriggerEnter(Collider c) {
		if (c.CompareTag("Player")) {
			Collected();
			this.audio.Play();
		}
	}
	
	void Collected() {
		StartCoroutine(CollectedAsync());
	}
	
	IEnumerator CollectedAsync() {
		Transform playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
		iTween.MoveTo(gameObject, playerTransform.position, 1.0f);
		iTween.ScaleBy(gameObject, Vector3.one * 0.2f, 0.8f);
		yield return new WaitForSeconds(1.05f);
		Destroy(gameObject);
	}
	
}
