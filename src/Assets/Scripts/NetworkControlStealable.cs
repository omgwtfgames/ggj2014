using UnityEngine;
using System.Collections;

public class NetworkControlStealable : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void BecomeOwner() {
	    var view = GetComponent<NetworkView>();
	    var newObjID = Network.AllocateViewID();
	 
	    networkView.RPC("ChangeOwner", RPCMode.Others, newObjID);
	 
	    view.viewID = newObjID;
		
		Debug.Log(gameObject.name + " now owned by " + (Network.isClient ? "client" : "server"));
	}
 
	[RPC]
	void ChangeOwner(NetworkViewID objID) {
	    var view = GetComponent<NetworkView>();
		view.viewID = objID;
		Debug.Log(gameObject.name + " stolen");
	}
}
