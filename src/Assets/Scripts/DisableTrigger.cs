using UnityEngine;
using System.Collections;

public class DisableTrigger : MonoBehaviour {
	public Knight kTarget;
	void OnTriggerEnter(Collider c) {
		if (c.CompareTag("Player"))
		{
			if (kTarget != null) { kTarget.triggered = false; kTarget.animation.CrossFade ("AC_wake",3f); }
			
			this.gameObject.SetActive (false);
			this.collider.enabled = false;
		}
		
	}
}
